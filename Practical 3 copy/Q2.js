function flexible(fOperation, operand1, operand2) {
    var result = fOperation(operand1, operand2);
    return result;
}
var output2 = "";
output2 += flexible(function (num1, num2) {return num1 + num2; }, 3, 5) + "<br/>";
output2 += flexible(function (num1, num2) {return num1 * num2; }, 3, 5) + "<br/>";
    document.getElementById("outputArea2").innerHTML = output2;