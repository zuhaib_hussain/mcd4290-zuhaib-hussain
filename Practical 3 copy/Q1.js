function objectToHTML (object)
{
    let result = ""
    for (prop in object)
    {
        result += prop + ": " + object[prop] + "\n"
    }
    return result
}

var testObj =
{ 
    number: 1,
    string: "abc",
    array: [5, 4, 3, 2, 1],
    boolean: true
};

let output = objectToHTML(testObj)
document.getElementById("outputArea1").innerText = output