/* declare a varable with an array
 * declare a function with the name extremeValues and the array varable as the parameter
 * * declare two varables maxValue and minValue with the values -Infinity and Infinity respactively
 * * declare for loop to run for the length if the array
 * * if value being assessed is greater then the value of maxValue then maxValue is equal to value being assessed
 * * if value being assessed is less then the value of minValue then minValue is equal to value being assessed
 */

function extremeValues(values)
{
    //let maxValue = Number.NEGATIVE_INFINITY;
    //let minValue = Number.POSITIVE_INFINITY;
    
    let maxValue = -Infinity;
    let minValue = Infinity;
    for(let i = 0; i <= values.length; i++) 
    {
        if(values[i] > maxValue) 
        {
            maxValue = values[i]
        }
        if(values[i] < minValue) 
        {
            minValue = values[i]
        }
    }
    return [maxValue, minValue]
}

var values = [4, 3, 6, 12, 1, 3, 8];

let output3 = extremeValues (values)
 document.getElementById("outputArea3").innerHTML = output3;