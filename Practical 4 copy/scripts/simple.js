function doIt()
{
    let num1Ref = document.getElementById("number1");
    let num2Ref = document.getElementById("number2");
    let num3Ref = document.getElementById("number3");
    let ansRef = document.getElementById("answer");
    
    let num1 = Number(num1Ref.value);
    let num2 = Number(num2Ref.value);
    let num3 = Number(num3Ref.value);
    let ans = num1 + num2 + num3;
    let ansString = ans.toString();
        
    if (ans > 0)
        {
            ansRef.className = "positive";
        }else{
            ansRef.className = "negative";
        }
    if(ans%2 === 0)
        {
            ansString += "(even)"
        }else{
            ansString += "(odd)"
        }
        ansRef.innerHTML = ansString;
}